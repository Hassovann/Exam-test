def raw_input(input_return):
    try:
        input_int=int(input_return)
        if not 0<input_int<10000:
            print ("Invalid number! please input a number between 0-10000")
            return None
        return input_int
    except ValueError:
            print ("The input was not a valid integer.")
            return None
while True:
    input_int=raw_input(int(input("Input a number between 0-10000 :  ")))
    if input_int:break

def convert(input_int):
    one_to_nineteen = ("","one ","two ","three ","four ","five ","six ","seven ","eight ","nine ","ten ","eleven ",
                        "twelve ","thirteen ","fourteen ","fifteen ","sixteen ","seventeen ","eighteen ","nineteen ")
    tens =("","","twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety")

    if input_int<20:
        return  one_to_nineteen[input_int] 

    if input_int==20:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    if input_int==30:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    if input_int==40:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    if input_int==50:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    if input_int==60:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    if input_int==70:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    if input_int==80:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    if input_int==90:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    elif input_int<100:
        return  tens[input_int // 10] + "-" +one_to_nineteen[int(input_int % 10)] 

    if input_int==100:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==200:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==300:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==400:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==500:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==600:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==700:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==800:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==900:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    elif input_int<1000:
        return one_to_nineteen[input_int // 100]  + "hundred " + "and " +convert(int(input_int % 100))

    if input_int==1000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==2000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==3000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==4000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==5000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==6000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==7000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==8000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==9000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    elif input_int<1000000: 
        return  convert(input_int // 1000) + "thousand" + ", " + convert(int(input_int % 1000))

print(convert(input_int))

