# Converting number to word
*examination

This is a converting function.

It converts a number to an English word with a specific condition of adding "," after thousand, "and" after hundreds, and "-" after tens.

And it has a function to ensure that the input integer is between 0-10000.

If the input integer is between 0-10000, it will be automatically converted to an English word through the above functionalty.

However, if it is not between 0-10000, "Invalid number! Please input a number between 0-10000" message will be shown.
 

# Usage
